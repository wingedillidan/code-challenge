const uuidv4 = require('uuid/v4');
let rhinoceroses = require('../state/data');

// private

function validateRhinocerus(data) {
  // validate keys
  const keys = Object.keys(data);
  if (keys.length !== rhinoceroses.keys.length || !keys.reduce((accum, key) => {
      return accum && rhinoceroses.keys.includes(key) >= 0 ? true : false;
    }, true)) {
      throw new Error('Rhinoceros does not meet strict identification criteria (missing/too many keys)')
  }

  // validate name
  if (!data.name) {
    throw new Error('Please name your Rhinocerus');
  } else if (data.name.length > 20) {
    throw new Error('Your Rhinoceros\'s name is too long! (max: 20 chars)');
  }

  // validate species
  if (!rhinoceroses.species.includes(data.species)) {
    throw new Error('That species is not on our list.');
  }

  return true;
}

// public

exports.getEndangered = (endangeredThreshold=2) => {
  // Create counts of each species
  const sortedBySpecies = rhinoceroses.slice().reduce((accum, rhino) => {
    if(!accum[rhino.species]) {
      accum[rhino.species] = [rhino];
    } else {
      accum[rhino.species].push(rhino);
    }

    return accum;
  }, {});

  // Create filtered list
  return Object.keys(sortedBySpecies).reduce((accum, species) => {
    if (sortedBySpecies[species].length <= endangeredThreshold) {
      return accum.concat(sortedBySpecies[species]);
    }

    return accum;
  }, []);
}

exports.getAll = (name, species) => {
  let all = rhinoceroses.slice();

  if (name) {
    if (!Array.isArray(name)) {
      name = [name];
    }
    all = all.filter(rhino => name.includes(rhino.name));
  }

  if (species) {
    if (!Array.isArray(species)) {
      species = [species];
    }
    all = all.filter(rhino => species.includes(rhino.species));
  }

  return all;
};

exports.get = id => {
  if (!id) {
    throw new Error('Please supply a rhino ID');
  }
  const i = rhinoceroses.findIndex(o => o.id === id);

  if (i >= 0) {
    return rhinoceroses[i];
  } else {
    throw new Error('Rhinoceros does not exist');
  }
}

exports.newRhinoceros = data => {
  if (!data || typeof(data) !== 'object' || Array.isArray(data)) {
    throw new Error('That\'s not a rhinoceros.');
  }
  data.id = uuidv4();

  validateRhinocerus(data);
  rhinoceroses.push(data);

  return data;
};
