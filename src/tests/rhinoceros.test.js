const model = require('../models/rhinoceros')

describe('get()', () => {
  it('should fail on invalid id', () => {
    expect(() => {
      model.get('thisshouldfail');
    }).toThrow('Rhinoceros does not exist');
  });

  it('should return the matching rhino', () => {
    expect(model.get('a832bb9e-f20b-4be5-94aa-cf7883939b67')).toEqual({
      id: 'a832bb9e-f20b-4be5-94aa-cf7883939b67',
      name: 'Stompy',
      species: 'javan_rhinoceros'
    });
  });
})

describe('newRhinocerus()', () => {
  it('should fail on no name', () => {
      expect(() => {
        model.newRhinoceros({species: 'black_rhinoceros', name: ''});
      }).toThrow('Please name your Rhinocerus');
  });

  it('should fail on long name', () => {
    expect(() => {
      model.newRhinoceros({
        species: 'black_rhinoceros',
        name: '12345678901234567890.'
      });
    }).toThrow('Your Rhinoceros\'s name is too long! (max: 20 chars)');
  });

  it('should fail on non-existant type', () => {
    expect(() => {
      model.newRhinoceros({
        species: 'purple',
        name: '12345678901234567890'
      });
    }).toThrow('That species is not on our list.');
  });

  it('should fail with empty object', () => {
    expect(() => {
      model.newRhinoceros({});
    }).toThrow('Rhinoceros does not meet strict identification criteria (missing/too many keys)');
  });

  it('should fail with extra key', () => {
    expect(() => {
      model.newRhinoceros({
        species: 'black_rhinoceros',
        name: '12345678901234567890',
        bongocat: 'plays bongos'
      });
    }).toThrow('Rhinoceros does not meet strict identification criteria (missing/too many keys)');
  });

  it('should fail with ABSOLUTELY NOTHING', () => {
    expect(() => {
      model.newRhinoceros();
    }).toThrow('That\'s not a rhinoceros.');
  });

  it('should pass this time', () => {
    expect(model.newRhinoceros({
      species: 'black_rhinoceros',
      name: '12345678901234567890'
    }))
      .toEqual({
        id: expect.any(String),
        species: 'black_rhinoceros',
        name: '12345678901234567890'
      });
  });
})
