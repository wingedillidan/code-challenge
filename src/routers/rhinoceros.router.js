const Router = require('koa-router'),
  router = new Router(
    {prefix: '/rhinoceros'}
  ),
  model = require('../models/rhinoceros');

router.get('/endangered', (ctx, next) => {
  const rhinoceroses = model.getEndangered();
  ctx.response.body = { rhinoceroses };
});

router.get('/:id', (ctx, next) => {
  try {
    ctx.response.body = model.get(ctx.params.id);
  } catch (e) {
    ctx.response.status = 400;
    ctx.response.body = e.message;
  }
});

router.get('/', (ctx, next) => {
  const rhinoceroses = model.getAll(ctx.query.name, ctx.query.species);
  ctx.response.body = { rhinoceroses };
});

router.post('/', (ctx, next) => {
  try {
    ctx.response.body = model.newRhinoceros(ctx.request.body);
  } catch (e) {
    ctx.response.status = 400;
    ctx.response.body = e.message;
  }
});

module.exports = router;
