const Router = require('koa-router'),
    RhinocerosRouter = require('./routers/rhinoceros.router')
    router = new Router({
        prefix: '/app/v1'
    });

router.use(RhinocerosRouter.routes(), RhinocerosRouter.allowedMethods());

module.exports = router;
